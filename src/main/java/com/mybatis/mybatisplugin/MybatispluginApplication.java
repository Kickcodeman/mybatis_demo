package com.mybatis.mybatisplugin;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan(basePackages ={ "com.mybatis.mybatisplugin.mapper"})
@EnableCaching
public class MybatispluginApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatispluginApplication.class, args);
    }

}
