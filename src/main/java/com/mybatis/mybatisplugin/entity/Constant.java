package com.mybatis.mybatisplugin.entity;

import com.mybatis.mybatisplugin.handler.BaseEnum;
import lombok.Getter;
import lombok.Setter;

public class Constant {

    public enum EntityStatus implements BaseEnum<EntityStatus,String> {

        LOGIN(100, "登陆"),
        ACTIVE(200,"激活");

        @Setter
        @Getter
        private Integer code;

        @Setter
        @Getter
        private String value;

        EntityStatus(Integer code, String value) {
            this.code = code;
            this.value = value;
        }

        public static void main(String[] args) {
            System.out.println(ACTIVE.getValue());
        }
    }

}
