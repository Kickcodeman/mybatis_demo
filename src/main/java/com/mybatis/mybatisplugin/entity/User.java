package com.mybatis.mybatisplugin.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class User implements Serializable {
    /**
     * id
     */
    private Integer id;

    /**
     * userName
     */
    private String userName;

    /**
     * age
     */
    private Integer age;

    /**
     * brithdate
     */
    private LocalDateTime brithdate;

    private Constant.EntityStatus entityStatus = Constant.EntityStatus.LOGIN;
}