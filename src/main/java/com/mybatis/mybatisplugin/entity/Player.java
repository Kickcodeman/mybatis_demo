package com.mybatis.mybatisplugin.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class Player implements Serializable {
    private Integer playerId;

    private Integer teamId;

    private String playerName;

    private Double height;

    private Constant.EntityStatus entityStatus = Constant.EntityStatus.LOGIN;
}