package com.mybatis.mybatisplugin.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mybatis.mybatisplugin.entity.Constant;
import com.mybatis.mybatisplugin.entity.Player;
import com.mybatis.mybatisplugin.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class PlayerController {

    private PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/getPlayer/{id}")
    public Player getPlayerById(@PathVariable("id") Integer id) {
        Player player = playerService.selectByPrimaryKey(id);
        return player;
    }

    @DeleteMapping("/delete/{id}")
    public void delterPlayer(@PathVariable("id") Integer playerId) {
        int id = playerService.deleteByPrimaryKey(playerId);
        System.out.println(id);
    }

    @GetMapping("/getAll")
    public List<Player> getPlayers() {
        List players = playerService.findAllPlayers();
        return players;
    }

    @GetMapping("/pageInfo/{pageNum}/{pageSize}")
    public PageInfo getPlayersByPage(@PathVariable("pageNum") int pageNum,
                                     @PathVariable("pageSize") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List players = playerService.findAllPlayers();
        return new PageInfo<List<Player>>(players);
    }

    @PostMapping("/insert")
    public int inserPlayer() {
        Player player = new Player();
        player.setEntityStatus(Constant.EntityStatus.ACTIVE)
        .setHeight(1.80)
        .setPlayerName("zempty"+ UUID.randomUUID())
        .setTeamId(1);
        return playerService.insert(player);
    }

    @PutMapping("/update/{playerId}")
    public int updatePlayer(@PathVariable("playerId") int playerId) {
        Player player = playerService.selectByPrimaryKey(playerId);
        player.setPlayerName("redis test");
        return playerService.updateByPrimaryKey(player);
    }

}
