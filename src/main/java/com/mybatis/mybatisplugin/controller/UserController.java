package com.mybatis.mybatisplugin.controller;

import com.mybatis.mybatisplugin.entity.Constant;
import com.mybatis.mybatisplugin.entity.User;
import com.mybatis.mybatisplugin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/getUser")
    public int addUser() {
        User user = new User();
        user.setAge(20)
        .setUserName("zempty")
        .setEntityStatus(Constant.EntityStatus.LOGIN)
        .setBrithdate(LocalDateTime.now());
        return userService.insert(user);
    }
}
