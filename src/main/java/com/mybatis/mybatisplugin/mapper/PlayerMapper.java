package com.mybatis.mybatisplugin.mapper;

import com.mybatis.mybatisplugin.entity.Player;
import com.mybatis.mybatisplugin.vo.PlayerVo;

import java.util.List;

//@Mapper
public interface PlayerMapper {

    int deleteByPrimaryKey(Integer playerId);

    int insert(Player record);

    int insertSelective(Player record);

    Player selectByPrimaryKey(Integer playerId);

    int updateByPrimaryKeySelective(Player record);

    int updateByPrimaryKey(Player record);

    List<Player> findAllPlayers();

    PlayerVo findPlayerById(Integer playerId);

}