package com.mybatis.mybatisplugin.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public abstract class BaseEnumTypeHandler<E extends BaseEnum> extends BaseTypeHandler<E>{

    private Class<E> type;

    private E[] enums;

    public BaseEnumTypeHandler() {

    }

    BaseEnumTypeHandler(Class<E> type) {
       if(type == null)
           throw new IllegalArgumentException("type argument can not ber null");
       this.type = type;
       this.enums = type.getEnumConstants();
       if(this.enums == null)
           throw new IllegalArgumentException(type.getSimpleName() + "does not represent an enum type");
    }

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, E parameter, JdbcType jdbcType) throws SQLException {
        if(jdbcType == null)
            preparedStatement.setString(i, Objects.toString(parameter.getValue()));
        else
            preparedStatement.setObject(i,parameter.getValue(),jdbcType.TYPE_CODE);

    }

    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String i = rs.getString(columnName);
        if(rs.wasNull())
            return null;
        else
            return locateEnumStatus(i);
    }

    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String i = rs.getString(columnIndex);
        if (rs.wasNull())
            return null;
        else
            return locateEnumStatus(i);

    }

    @Override
    public E getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        String i = callableStatement.getString(columnIndex);
        if(callableStatement.wasNull())
            return null;
        else
            return locateEnumStatus(i);
    }


    private E locateEnumStatus(String value) {
        for (E e : enums) {
            if (Objects.toString(e.getValue()).equals(value)) {
                return e;
            }
        }
        throw new IllegalArgumentException("未知的枚举类型：" + value + ",请核对"
                + type.getSimpleName());
    }
}
