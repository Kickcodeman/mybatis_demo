package com.mybatis.mybatisplugin.handler;

import com.mybatis.mybatisplugin.entity.Constant;
import org.apache.ibatis.type.MappedTypes;

public class EntityEnumTypeHandler<E extends BaseEnum > extends BaseEnumTypeHandler<E> {
    public EntityEnumTypeHandler(Class<E> type) {
        super(type);
    }
}
