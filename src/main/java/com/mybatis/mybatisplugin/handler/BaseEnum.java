package com.mybatis.mybatisplugin.handler;

public interface BaseEnum<E extends Enum,T>{
    T getValue();
}
