package com.mybatis.mybatisplugin.service;

import com.mybatis.mybatisplugin.entity.Player;
import com.mybatis.mybatisplugin.mapper.PlayerMapper;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@CacheConfig(cacheNames = {"player"})
public class PlayerService{

    private PlayerMapper playerMapper;

    @Autowired
    public PlayerService(PlayerMapper playerMapper) {
        this.playerMapper = playerMapper;
    }

   //测试事务的使用
    @Transactional
    public int deleteByPrimaryKey(Integer playerId) {
        int num = playerMapper.deleteByPrimaryKey(playerId);
        int c = 3/0;
        return num;
    }

    
    public int insert(Player record) {
        return playerMapper.insert(record);
    }

    
    public int insertSelective(Player record) {
        return playerMapper.insertSelective(record);
    }

    @Cacheable(key = "#playerId")
    public Player selectByPrimaryKey(Integer playerId) {
        return playerMapper.selectByPrimaryKey(playerId);
    }

   @Transactional
    public int updateByPrimaryKeySelective(Player record) {
        return playerMapper.updateByPrimaryKeySelective(record);
    }

    @CacheEvict(key = "#record.playerId",beforeInvocation = true)
    public int updateByPrimaryKey(Player record) {
        System.out.println("方法进来了");
        return playerMapper.updateByPrimaryKey(record);
    }

    public List<Player> findAllPlayers() {
        return playerMapper.findAllPlayers();
    }
}
